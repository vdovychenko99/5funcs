//
// Created by viktor vdovychenko on 11/7/18.
//

int getopt_long (int argc, char ** argv, const char *short_opts, const
struct option *long_opts, int *opt_index);

struct option
{
    char *long_opts;
    int with_arg;
    int *opt_flag;
    int short_opt;
};