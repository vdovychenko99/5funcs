#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
const char usage_str[] = "usage: getopt options\n"
"Options:\n"
" -v - option p\n"
" -p - option q\n"
" -q arg - option q\n"
" -r - option r\n "
"    --version - version\n"
"    --print   - option p\n"
" --name=arg - option name\n";
void version ()
{
    printf ("getopt test program - version 1.0\nby Alexander Dovhal\n") ;
}
void print_p () {
    printf ("Option 'p' used\n") ;
}
int main (int argc, char **argv) {
    int opt ;
    struct option long_options[] =
            {
                     {"version", 0, 0, 'v'},
                     {"print", 0, 0, 'p'},
                     {"name", 1, 0, 0x80},
                      {0,          0,0,0 }};


                            if (argc < 2) {
                                printf     (usage_str) ;
                                exit (0) ; }
                            while (1)

    {
        int option_index ;
        opt = getopt_long(argc, argv, "vpq:r",
                          long_options, &option_index);
        if (opt == -1)
            break;
        switch (opt)
        {
            case 0:
                printf ("%s: used long option --%s", argv[0],long_options [option_index].name) ;
             if (optarg)
                printf ("with argument: %s", optarg) ;
             printf ("\n") ;
            break ;
            case 'v':
                version ();
                break ;
                case 'p':
                print_p ();
                break ;
                case 'q':
                printf ("Option 'q' used, with argument: %s\n", optarg) ;
                break ;
                case 'r':
                printf ("Option 'r' used\n") ; break ;
            case 0x80: //name
                printf ("Long Option --name is used"
                        " with arg: %s\n", optarg) ;
                break ;
            default:
                fprintf (stderr, "Warning: unknown option\n") ;
                fprintf (stderr, usage_str) ;
                exit (1) ;
        } }
    return 0 ; }
